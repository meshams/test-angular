import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AllService } from '../services/all.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(public allService: AllService, public router: Router) {}
  canActivate(): boolean  {
    if (this.allService.isLoggedIn !== true) {
      window.alert("Access not allowed!");
      this.router.navigate(['login'])
    }
    return true;
  }
  
}
