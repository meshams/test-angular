import { Component } from '@angular/core';
import { AllService } from '../services/all.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  
  constructor(public allService: AllService) { }
  logout() {
    this.allService.doLogout()
  }
}
