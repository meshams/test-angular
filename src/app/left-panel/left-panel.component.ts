import { Component } from '@angular/core';

@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.css']
})
export class LeftPanelComponent {

  showMore = false;
  menuList: any;
  notification: any;
  notificationLength:any
  selected: any = {};

  constructor() {
    this.menuList = [
      { name: "Home", route: "", icon: "fa fa-home" },
      {
        name: "Production", route: "", icon:"fa fa-cogs", subMenu: [
          { label: "Dying", route: "" },
          { label: "Finishing", route: "" },
          { label: "Crusting", route: "" },
        ]
      },
      { name: "Reports", route: "", icon: "fa fa-file" },
      { name: "Planning", route: "", icon: "fa fa-eye" },
      { name: "Finances", route: "", icon: "fa fa-inr" },
    ];
    this.notification = [
      {tittle: "Some placeholder content in a paragraph below the heading and date.", time: "45 minutes ago"},
      {tittle: "Mirza International added a new documents of Buffcow T1 color", time: ""},
      {tittle: "Some placeholder content in a paragraph below the heading and date.", time: "45 minutes ago"},
      {tittle: "Some placeholder content in a paragraph below the heading and date.", time: "45 minutes ago"},
      {tittle: "Some placeholder content in a paragraph below the heading and date.", time: "45 minutes ago"},
      {tittle: "Some placeholder content in a paragraph below the heading and date.", time: "45 minutes ago"},
    ]
  }

  ngOnInit(){
    this.notificationLength = this.notification.length;
  }

  select(type: any, item: any, $event: any) {
    this.selected[type] = (this.selected[type] === item ? null : item);
    console.log('Item: ', item);
    console.log('Itema: ', this.selected[type]);
    $event ? $event.stopPropagation() : null;
  }
  isActive(type: any, item: any) {
    return this.selected[type] === item;
  }

  onShow () {
    this.showMore = !this.showMore;
  }
}
