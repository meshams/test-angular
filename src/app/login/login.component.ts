import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from  '@angular/forms';
import { Router } from '@angular/router';
import { AllService } from '../services/all.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  
  submitted = false;
  userLog: any;
  form!: FormGroup;
  errMsg: string ="";
  isLoading = false;

  constructor(
    private fb: FormBuilder, 
    private allService: AllService, 
    private _route:Router
    ) {}

  ngOnInit(): void {
    this.userLog = localStorage.getItem('currentUser');
    this.UserLoginFunction(this.userLog);
    this.form = this.fb.group({   
        email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
        password: ['',[ Validators.required, Validators.minLength(6), Validators.maxLength(25), ]],
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  onSubmit(): void {
    this.submitted = true;
    if (this.form.invalid) {  return;  }
    this.isLoading = true;
    this.allService.logindata(this.form).subscribe((res:any) =>{  
      //console.log(res); 
      this.isLoading = false;  
      if(res !== null){
        this.errMsg="";
        this.UserLoginFunction(res);
      } 
      else{
        this.errMsg="Invalid credentials";
        this.isLoading = false;
      }
       
    })
  }

  UserLoginFunction(isUser:any){
    if(isUser){           
      this._route.navigate(['dashboard']);        
    }else{
      this._route.navigate(['login']);
    } 
  }
}
