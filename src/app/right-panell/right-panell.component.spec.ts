import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RightPanellComponent } from './right-panell.component';

describe('RightPanellComponent', () => {
  let component: RightPanellComponent;
  let fixture: ComponentFixture<RightPanellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RightPanellComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RightPanellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
