import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, map, throwError } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AllService {

  userData: any;
  isUser:any
  apiUrl = `../../assets/Code_Test.postman_collection.json`;

  constructor(private _http:HttpClient, public router: Router) { }

  logindata(login:any){    
    return this._http.get<any>(this.apiUrl)
    .pipe(map(user => {
      this.userData = JSON.parse(user.item[0].request.body.raw); 
      if(this.userData.email == login.value.email && this.userData.password == login.value.password){ 
        localStorage.setItem("currentUser",JSON.stringify(this.userData.email));         
        return this.userData;       
      }else{
        return null;        
      }   
    }),
    catchError(this.handleError));
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('currentUser');
    return authToken !== null ? true : false;
  }

  doLogout() {
    let removeUser = localStorage.removeItem('currentUser');
    if (removeUser == null) {
      this.router.navigate(['login']);
    }
  }

  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {      
      msg = error.error.message;
    } else {
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }

}
